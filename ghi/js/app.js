function createCard(
  name,
  description,
  pictureUrl,
  startDate,
  endDate,
  location
) {
  return `
    <div class="col p-3">
      <div class="card">
        <div class="shadow p-3 mb-0 bg-white rounded">
            <img src="${pictureUrl}" class="card-img-top">
            <div class="card-body">
                <h5 class="card-title">${name}</h5>
                <p class="card-text"><small class="text-muted">${location}</small></p>
                <p class="card-text">${description}</p>
            </div>
            <div class="card-footer">${startDate} - ${endDate}
            </div>
            
        </div>
      </div>

    
</div>
    `;
}

window.addEventListener("DOMContentLoaded", async () => {
  const url = "http://localhost:8000/api/conferences/";

  try {
    const response = await fetch(url);

    if (!response.ok) {
      // Figure out what to do when the response is bad
      showError("Failed to fetch conferences");
    } else {
      const data = await response.json();

      for (let conference of data.conferences) {
        const detailUrl = `http://localhost:8000${conference.href}`;
        const detailResponse = await fetch(detailUrl);
        if (detailResponse.ok) {
          const details = await detailResponse.json();
          const location = details.conference.location.name;
          const title = details.conference.name;
          const description = details.conference.description;
          const pictureUrl = details.conference.location.picture_url;
          let startDate = new Date(
            details.conference.starts
          ).toLocaleDateString();
          let endDate = new Date(details.conference.ends).toLocaleDateString();
          const html = createCard(
            title,
            description,
            pictureUrl,
            startDate,
            endDate,
            location
          );
          const column = document.querySelector(".row");
          column.innerHTML += html;
        }
      }
    }
  } catch (e) {
    // Figure out what to do if an error is raised

    showError("Failed to load data");
  }
});

function showError(message) {
  const alertHTML = `
        <div class="alert alert-primary" role="alert">
        ${message}
        </div>
        `;
  document.body.innerHTML += alertHTML;
}
