import Nav from './Nav';
import AttendeesList from './AttendeesList';
import LocationForm from './LocationForm';
import ConferenceForm from './ConferenceForm';
import AttendeeForm from './AttendeesForm'
import PresentationForm from './PresentationForm';
import MainPage from './MainPage';

import { BrowserRouter, Routes, Route } from "react-router-dom"

function App(props) {
  if (props.attendees === undefined) {
    return null;
  }
  return (
    <>
    <BrowserRouter>
    <Nav />
    <Routes>
      <Route path= "/" element={<MainPage />}></Route>
      <Route path="/locations/new" element={<LocationForm />}></Route>
      <Route path="/conferences/new" element={<ConferenceForm />}></Route>
      <Route path="/presentations/new" element={<PresentationForm />}></Route>
      <Route path="/attendees">
        <Route index element={<AttendeesList attendees={props.attendees} />}></Route>
        <Route path="new" element={<AttendeeForm />}></Route>
      </Route>
    </Routes>
    </BrowserRouter>
    
    
      {/*<AttendeeForm /> */}
      {/* <ConferenceForm /> */}
      {/* <LocationForm /> */}
    {/* <AttendeesList attendees={props.attendees} /> */}
    
    </>
  );
}

export default App;
